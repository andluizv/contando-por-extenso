//Variáveis:
const unitNumbers = {1: 'um', 2: 'dois', 3: 'três', 4: 'quatro', 5: 'cinco', 6: 'seis', 7: 'sete', 8: 'oito', 9: 'nove', 0: 'zero'};
const tenToTwenty = {10: 'dez', 11: 'onze', 12: 'doze', 13: 'treze', 14: 'quatorze', 15: 'quinze', 16: 'dezesseis', 17: 'dezessete', 18: 'dezoito', 19: 'dezenove', 20: 'vinte'};
const twentyToHundred = {2: 'vinte', 3: 'trinta', 4: 'quarenta', 5: 'cinquenta', 6: 'sessenta',7: 'setenta', 8: 'oitenta', 9: 'noventa'}
const hundredToThousand = {1: 'cento', 2: 'duzentos', 3: 'trezentos', 4: 'quatrocentos', 5: 'quinhentos', 6: 'seiscentos', 7: 'setecentos', 8: 'oitocentos', 9: 'novecentos', 100: 'cem'}
const numberInput = document.querySelector('#numberInput');
const numberRanged = document.querySelector('#numberRanged');
const btnContar = document.querySelector('#countStart');
const boardParent = document.querySelector('.quadro');


//Funções:
function ate99(contar){
    if (contar < 10){
        return unitNumbers[contar];
        
    }
    if (contar <= 20){
        return tenToTwenty[contar];
        
    }
    if (contar <= 99){
        let dezena = contar.toString();
        if (contar % 10 == 0){
            return twentyToHundred[dezena[0]];
            
        }
        return `${twentyToHundred[dezena[0]]} e ${unitNumbers[dezena[1]]}`;
        
    }
}
const createSpan = (insertText) => {
    const span = document.createElement('span');
    
    span.innerText = insertText + ', ';
    boardParent.appendChild(span);
}
const createLastSpan = (insertText) => {
    const span = document.createElement('span');
    
    span.innerText = insertText;
    boardParent.appendChild(span);
}
const escrevePorExtensoAte = (ateQuanto) => {
    
    if (ateQuanto > 1000){
        alert('Desculpe, mas só sei contar até 1000 nesse momento!');
        return;
    }
    if (ateQuanto < 0){
        alert('Desculpa, mas é só pedir o número positivo e escrever menos na frente!');
        return;
    }
    for (let i = 0; i <= ateQuanto; i++){
        let unidade = i;
        if (unidade < 10){
            let conteudo = unitNumbers[unidade];
            createSpan(conteudo);
            continue;
        }
        if (unidade <= 20){
            let conteudo = tenToTwenty[unidade];
            createSpan(conteudo);
            continue;
        }
        if (unidade <= 99){
            let dezena = unidade.toString();
            if (unidade % 10 == 0){
                let conteudo = twentyToHundred[dezena[0]]
                createSpan(conteudo);
                continue;
            }
            let conteudo = `${twentyToHundred[dezena[0]]} e ${unitNumbers[dezena[1]]}`
            createSpan(conteudo);
            continue;
        }
        if (unidade == 100){
            let conteudo = hundredToThousand[unidade];
            createSpan(conteudo);
            continue;
        }
        if (unidade <= 999){
            let centena = unidade.toString();
            let dezenas = (centena[1]+centena[2])*1;
            
            if (unidade % 100 == 0){
                let conteudo = hundredToThousand[centena[0]];
                createSpan(conteudo);
                continue;
            }
            let conteudo = `${hundredToThousand[centena[0]]} e ${ate99(dezenas)}`;
            createSpan(conteudo);
        }
        if (unidade == 1000){
            let conteudo = 'mil'
            createSpan(conteudo);
        }
    }
}

//Eventos:
numberInput.addEventListener('input', () => {
    numberRanged.value = numberInput.value;
})

numberRanged.addEventListener('input', () => {
    numberInput.value = numberRanged.value;
})

btnContar.addEventListener('click', () => {
    boardParent.innerHTML = '';
    let desiredNumber = numberInput.value;
    escrevePorExtensoAte(desiredNumber);
    let toRemoveLast = boardParent.lastChild.textContent;
    toRemoveLast = toRemoveLast.replace(",", "! Pronto pessoa, pode conferir!");
    boardParent.removeChild(boardParent.lastElementChild);
    createLastSpan(toRemoveLast);
});